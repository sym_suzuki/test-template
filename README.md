# test-template

テストをするためのひな形です。

package.jsonとbower.jsonに必要なライブラリが定義されています。

* package.json  
主にコマンドライン（node.js）から使用するライブラリが定義されています。

* bower.json
主にブラウザから使用するライブラリが定義されています。

## 下準備

まずはnode.jsをインストールしてください。  
windowsならインストーラが用意されてるので簡単です。

    http://nodejs.org/

node.jsのインストールが終わったらbowerというツールをインストールします。  
コマンドプロンプト（または好きなシェル）を開いて以下のコマンドを打ちます。

    npm install -g bower

* npm  
node.jsのパッケージマネージャでnode.js専用のyumのようなものです。  
これでインストールするものは一部ブラウザでも動きますが基本的にnode.js用です。

* bower  
node.jsで動くブラウザ上のJavaScriptのパッケージマネージャです。  
これでインストールするものはブラウザで動かせます。  
コマンドでローカルにダウンロードしてくれるので、使うときは普通にscriptタグやlinkタグでHTMLから読み込む必要があります。  
（ダウンロードされたうちのどれを読み込めばよいのかなどはライブラリごとに違うので調べてください...）

## テストライブラリのインストール

このプロジェクトのディレクトリに移動して以下のコマンドを打ちます。

    npm install
    bower install

これらのコマンドでpackage.json, bower.jsonで指定されているライブラリがインストールできます。便利ですね。

## テストライブラリ

### QUnit
    http://qunitjs.com/
シンプルなTDD向けテストライブラリ。  
主にブラウザ上で使うがコマンドラインからも実行できるらしい。

### mocha, chai
    http://visionmedia.github.io/mocha/
    http://chaijs.com/
mochaはシンプルなテストフレームワーク。  
chaiはmochaとセットで使われるアサーションライブラリ。    
ブラウザでもnode.jsでも実行できる。

### jasmine
    http://jasmine.github.io/
    http://jasmine.github.io/2.0/introduction.html
ブラウザで動くBDDフレームワーク。  
コマンドラインから実行するにはRubyが必要らしい。